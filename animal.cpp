///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "animal.hpp"

using namespace std;

namespace animalfarm {

// Print out animal information	
void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


// Convert Gender enum into string
string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};

	
// Convert Color enum into string
string Animal::colorName (enum Color color) {
   switch (color) {
      case RED:		return string("Red"); break;
      case ORANGE:	return string("Orange"); break;
      case YELLOW:	return string("Yellow"); break;
      case GREEN:	return string("Green"); break;
      case BLUE:	return string("Blue"); break;
      case WHITE:	return string("White"); break;
      case BLACK:	return string("Black"); break;
      case BROWN:	return string("Brown"); break;
      case SILVER:	return string("Silver"); break;
   }

   return string("Unknown");
};

	
} // namespace animalfarm
