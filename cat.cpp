///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Max Mochizuki <maxmm@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   02/26/2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "cat.hpp"

using namespace std;

namespace animalfarm {

// Define traits for Cat	
Cat::Cat( string newName, enum Color newColor, enum Gender newGender ) {
	gender = newGender;
	species = "Felis catus";
	hairColor = newColor;
	gestationPeriod = 60;
	name = newName;
}

// Print out what Cat sounds like
const string Cat::speak() {
	return string( "Meow" );
}


/// Print our Cat and name first... then print whatever information Mammal holds.
void Cat::printInfo() {
	cout << "Cat Name = [" << name << "]" << endl;
	Mammal::printInfo();
}

} // namespace animalfarm
