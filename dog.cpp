///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file dog.cpp
/// @version 1.0
///
/// Exports data about all dogs
///
/// @author Max Mochizuki <maxmm@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   02/26/2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "dog.hpp"

using namespace std;

namespace animalfarm {

// Define traits for Dog
Dog::Dog( string newName, enum Color newColor, enum Gender newGender ) {
        gender = newGender;
        species = "Canis lupus";
        hairColor = newColor;
        gestationPeriod = 64;
        name = newName;
}

// Print out what Dog sounds like
const string Dog::speak() {
        return string( "Woof" );
}


/// Print out Dog information and then Animal information
void Dog::printInfo() {
        cout << "Dog Name = [" << name << "]" << endl;
        Mammal::printInfo();
}

} // namespace animalfarm

